# Kali Linux VM Provisioning with Ansible and Vagrant

This project automates the setup and provisioning of Kali Linux VMs for different architectures and purposes using Ansible and Vagrant.

## Structure

The project is structured as follows:

```
kali-setup/
├── inventory/
│ ├── hosts.ini
├── group_vars/
│ ├── all.yml
├── host_vars/
│ ├── x86_64.yml
│ ├── arm.yml
├── roles/
│ ├── common/
│ ├── wireless/
│ ├── security_assessment/
│ ├── reverse_engineering/
├── playbooks/
│ ├── setup_kali_x86_64.yml
│ ├── setup_kali_arm.yml
└── ansible.cfg
```


## Configuration Files

- **ansible.cfg**: This is the Ansible configuration file that specifies default settings for the playbook execution.
- **inventory/hosts.ini**: Contains the details of the Kali VM(s) that Ansible will manage.
- **group_vars/all.yml**: Defines global variables that will be used across different roles.
- **host_vars/x86_64.yml** and **host_vars/arm.yml**: Specify variables that are specific to each VM architecture.

## Roles

Roles are organized by their purpose and include all necessary tasks, handlers, templates, files, vars, defaults, and meta information.

- **common**: This role contains tasks and configurations that are common to all VMs, regardless of their specific purpose or architecture.
- **wireless**: Specific configurations and tasks for setting up VMs for wireless testing.
- **red_team**: Setups tailored for red team activities, including penetration testing and security assessments.
- **security_assessment**: Configurations and tasks for conducting security assessments.
- **reverse_engineering**: Tools and configurations for reverse engineering.

## Playbooks

Playbooks are defined for setting up Kali Linux on different architectures:

- **setup_kali_x86_64.yml**: Playbook for provisioning x86_64 VMs.
- **setup_kali_arm.yml**: Playbook for provisioning ARM VMs.

## Vagrant Integration

A `Vagrantfile` is provided to automate the setup of an x86_64 VM. It handles the creation of the VM, network setup, and provisioning with Ansible.

## Usage

To use this project, clone the repository and adjust the inventory files and variables to match your environment. Then, run the appropriate playbook for your target VM architecture:

```bash
ansible-playbook -i inventory/hosts.ini playbooks/setup_kali_x86_64.yml
```

for ARM VMs:

```bash
ansible-playbook -i inventory/hosts.ini playbooks/setup_kali_arm.yml
```

To use Vagrant for provisioning an x86_64 VM:

```bash
vagrant up
```

If you encounter

```
fatal: [arm-vm-1]: FAILED! => {"msg": "Missing sudo password"}
```

Use the `-K` flag.

## Contributing

Contributions are welcome. Please ensure you follow the project's coding and commit message guidelines.
